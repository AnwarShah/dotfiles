set encoding=utf8
set nocp "no compatible
call plug#begin('~/.vim/bundle')

"Plug 'Valloric/YouCompleteMe', { 'do': './install.py' }
Plug 'junegunn/fzf.vim'
Plug 'junegunn/fzf', { 'do': './install --all' }
Plug 'kchmck/vim-coffee-script'
Plug 'cakebaker/scss-syntax.vim'

call plug#end()
let g:plug_shallow=1

" Disable sensible on neovim
let g:pathogen_blacklist = []

if has('nvim')
  call add(g:pathogen_blacklist, 'vim-sensible')
endif

execute pathogen#infect()
syntax on
filetype plugin indent on

" Set tab to 4 spaces
set tabstop=4
set shiftwidth=2 smarttab expandtab softtabstop=0
set nowrap
" Show lin numbers by default
set nu!
set nowrap
set termguicolors
set background=dark

" Use powerline fonts
let g:airline_powerline_fonts = 1
" Smarter tabline with multiple buffer
let g:airline#extensions#tabline#enabled = 1

"Setting colorscheme
colorscheme gruvbox
let g:airline_theme='onedark'
set guifont=Hack\ 11

"Reducing vim refresh time for git gutter
set updatetime=250

" For ctrlp
set runtimepath^=~/.vim/bundle/ctrlp.vim
let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)|tmp|cache$'

let g:python_host_prog  = '/usr/bin/python2'
let g:python2_host_prog = '/usr/bin/python2'
let g:python3_host_prog = '/usr/bin/python3'

"Explicitly set python interpreter for ycmd server
let g:ycm_server_python_interpreter="/usr/bin/python2"

" Mapping selecting mappings
nmap <leader><tab> <plug>(fzf-maps-n)
xmap <leader><tab> <plug>(fzf-maps-x)
omap <leader><tab> <plug>(fzf-maps-o)

" Insert mode completion
imap <c-x><c-k> <plug>(fzf-complete-word)
imap <c-x><c-f> <plug>(fzf-complete-path)
imap <c-x><c-j> <plug>(fzf-complete-file-ag)
imap <c-x><c-l> <plug>(fzf-complete-line)

" Advanced customization using autoload functions
inoremap <expr> <c-x><c-k> fzf#vim#complete#word({'left': '15%'})


