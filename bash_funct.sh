#!/bin/bash

current_desktop="$XDG_CURRENT_DESKTOP"

function wwan_reset {
  nmcli radio wwan off
  sleep 2
  nmcli radio wwan on
}

function reconnect-modem {
  nmcli device disconnect ttyUSB0
  sleep 1
  nmcli device connect ttyUSB0
}

function nm_reset {
  sudo systemctl restart NetworkManager
}

function set-font {
  if [ x$current_desktop = "xMATE" ]
  then
      gsettings set org.mate.interface font-name "$1"
  elif [ x$current_desktop = "xX-Cinnamon" ]
  then
      gsettings set org.cinnamon.desktop.interface font-name "$1"
  else
      gsettings set org.gnome.desktop.interface font-name "$1"
  fi
}

function set-theme {
  if [ x$current_desktop = "xX-Cinnamon" ]
	then
	    gsettings set org.cinnamon.desktop.interface gtk-theme "$1"
	    gsettings set org.cinnamon.desktop.wm.preferences theme "$1"
  elif [ x$current_desktop = "xMATE" ]
	then
    	gsettings set org.mate.interface gtk-theme "$1"
  elif [ x$current_desktop = "xXFCE" ]
  	then
    	xfconf-query -c xsettings  -p /Net/ThemeName -s "$1"
  else
		gsettings set org.gnome.desktop.interface gtk-theme "$1"
  fi
}

function set-gs-theme {
  dconf write /org/gnome/shell/extensions/user-theme/name "'$1'"
}

function set-icon {
  if [ x$current_desktop = "xX-Cinnamon" ]
  then
      gsettings set org.cinnamon.desktop.interface icon-theme "$1"
  elif [ x$current_desktop = "xMATE" ]
  then
      gsettings set org.mate.interface icon-theme "$1"
  elif [ x$current_desktop = "xXFCE" ]
	then
      xfconf-query  -c xsettings -p /Net/IconThemeName -s "$1"
	else
      gsettings set org.gnome.desktop.interface icon-theme "$1"
  fi
}

function start_rubydoc {
  cd ~/.rvm/src/$(rvm current)/
  yard server
}

function start_gemdoc {
  cd ~/.rvm/gems/$(rvm current)/gems/
  all_versions_of_gem=$(ls -d --sort=version --reverse $1-[0-9.]* 2>/dev/null)

  # check for wrong gem name
  if [ "$?" -eq "2" ]; then
    echo "Error: No gem found with specified name!"
    return -1
  fi

  # select the dir with highest version
  gemdir=$(echo $all_versions_of_gem | head -n1 | cut -d ' ' -f 1)

  if cd $gemdir 2>/dev/null; then
    echo "starting yard server..."
    yard server
  else
    echo "An error occurred while trying to navigate to the gem dir $gemdir"
  fi
}

function ping_test {
  ping 8.8.8.8
}

function gem_server {
  yard server --gems
}

function update-repo() {
    for source in "$@"; do
        sudo apt-get update -o Dir::Etc::sourcelist="sources.list.d/${source}" \
        -o Dir::Etc::sourceparts="-" -o APT::Get::List-Cleanup="0"
    done
}

function compile_schema() {
  sudo glib-compile-schemas /usr/share/glib-2.0/schemas
}
