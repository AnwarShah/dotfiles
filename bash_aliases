alias suod=sudo
alias mv="mv -v"
alias cp="cp -v"
alias rm="rm -v"
alias "apt"="sudo apt"
alias "pgrep"="pgrep --list-full"
alias "tar"="tar --warning=no-unknown-keyword"
alias grep="grep -i"
alias rm="rm --one-file-system" #don't follow links
alias zoo="zeus"
alias s="sudo"
